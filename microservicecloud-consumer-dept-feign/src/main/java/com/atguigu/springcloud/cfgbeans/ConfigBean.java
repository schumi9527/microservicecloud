package com.atguigu.springcloud.cfgbeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
//feign的负载均衡使用的是robbin的负载均衡算法
@Configuration
public class ConfigBean //boot -->spring   applicationContext.xml --- @Configuration配置   ConfigBean = applicationContext.xml
{ 

	//33.改变robbin的负载均衡算法
	//达到的目的，用我们重新选择的随机算法替代默认的轮询。
	@Bean
	public IRule myRule()
	{
		//return new RoundRobinRule();//轮询
		return new RandomRule();//随机
		//return new RetryRule();//默认是轮询的，当有服务宕机时，访问几次后就不会访问宕机的，回头访问能正常使用的服务上
	}
}

//@Bean
//public UserServcie getUserServcie()
//{
//	return new UserServcieImpl();
//}
// applicationContext.xml == ConfigBean(@Configuration)
//<bean id="userServcie" class="com.atguigu.tmall.UserServiceImpl">